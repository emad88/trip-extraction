package trip;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.ZonedDateTime;
import java.util.stream.Stream;

class TripUtilTest {


    @ParameterizedTest
    @MethodSource("getTimes")
    void moreThanThreeMinutesShouldReturnFalse(ZonedDateTime t1, ZonedDateTime t2, boolean expected) {

        Assertions.assertThat(TripUtil.isTimeDifferenceLessThanThreeMinutes(t1, t2)).isEqualTo(expected);
    }

    private static Stream<Arguments> getTimes() {
        return Stream.of(
                Arguments.of(ZonedDateTime.now(), ZonedDateTime.now().minusMinutes(4), false),
                Arguments.of(ZonedDateTime.now(), ZonedDateTime.now().minusMinutes(1), true),
                Arguments.of(ZonedDateTime.now().minusMinutes(5), ZonedDateTime.now(), false),
                Arguments.of(ZonedDateTime.now().minusMinutes(2), ZonedDateTime.now(), true),
                Arguments.of(ZonedDateTime.now().minusMinutes(3), ZonedDateTime.now(), false)
        );
    }

    @ParameterizedTest
    @MethodSource("getDistances")
    void calculatedGPSDistanceIsCorrect(double lat1, double lon1, double lat2, double lon2, double expected) {

        Assertions.assertThat(TripUtil.distance(lat1, lon1, lat2, lon2)).isEqualTo(expected);
    }

    private static Stream<Arguments> getDistances() {
        return Stream.of(
                Arguments.of(54.69916, 25.21887, 54.6991616, 25.2190616, 12),
                Arguments.of(54.6991366, 25.2200466, 54.6991283, 25.2201266, 5),
                Arguments.of(54.6991366, 25.2200466, 54.6991366, 25.2200466, 0),
                Arguments.of(54.700025, 25.26005, 54.6999899, 25.2599999, 5),
                Arguments.of(54.6999899, 25.2599999, 54.6995583, 25.2595533, 56)

        );
    }
}
