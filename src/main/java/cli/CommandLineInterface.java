package cli;

import org.apache.commons.cli.Options;

public class CommandLineInterface {

    public Options buildOptions(){
        return new Options()
                .addRequiredOption("f",
                        "csv-file",
                        true,
                        "The absolute path to the CSV file to parse.");
    }
}
