package csv;

import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import trip.GPSCoordinates;
import trip.OriginalOdometer;
import trip.RecordData;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CSVRecordParser {

    private final static Logger LOGGER = LoggerFactory.getLogger(CSVRecordParser.class);
    private final static String TIMESTAMP = "timestamp";
    private final static String ODOMETER = "Odometer(meters)";
    private final static String GPS_COORDINATES = "GPS coordinates";
    private final static String ADDITIONAL_PROPERTIES = "Additional properties";

    public List<RecordData> processCSVData(List<CSVRecord> csvRecords) {

        List<RecordData> recordData = csvRecords.stream()
                .map(mapToRecordData)
                .collect(Collectors.toList());

        return recordData;
    }

    private Function<CSVRecord, RecordData> mapToRecordData = (record) -> {
        RecordData recordData = new RecordData();

        recordData.setTimestamp(ZonedDateTime.parse(record.get(TIMESTAMP)));
        recordData.setOdometer(Long.valueOf(record.get(ODOMETER)));
        recordData.setGpsCoordinates(gpsCoordinates(record));
        recordData.setAdditionalProperties(extractOriginalOdometer(record));

        return recordData;
    };

    private GPSCoordinates gpsCoordinates(CSVRecord record) {
        String gpsRecord = record.get(GPS_COORDINATES);
        String[] coordinates = gpsRecord.split(",");
        return new GPSCoordinates(Double.valueOf(coordinates[0]), Double.valueOf(coordinates[1]));
    }

    //TODO This is kinda hard coded, need to make it better
    private OriginalOdometer extractOriginalOdometer(CSVRecord record) {
        String additionalProperties = record.get(ADDITIONAL_PROPERTIES);
        Long extractedValue = null;
        if (additionalProperties.contains("null") || !additionalProperties.contains("orig_odo")) {
            return null;
        }

        try {
            String numbers = additionalProperties.replaceAll("\\D+", "");
            extractedValue = Long.valueOf(numbers);
        } catch (Exception e) {
            LOGGER.error("Could not extract original odometer from additional properties: " + e.getMessage());
        }

        return new OriginalOdometer(extractedValue);
    }
}
