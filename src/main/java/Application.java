import cli.CommandLineInterface;
import org.apache.commons.cli.*;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import trip.TripExtraction;

import java.io.FileReader;
import java.io.IOException;
import java.util.List;

public class Application {

    public static void main(String[] args) {

        Options options = new CommandLineInterface().buildOptions();

        try {

            CommandLine cmd = new DefaultParser().parse(options, args);
            FileReader fileReader = new FileReader(cmd.getOptionValue("f"));
            CSVParser csvParser = new CSVParser(fileReader, CSVFormat.DEFAULT
                    .withFirstRecordAsHeader()
                    .withDelimiter(';')
                    .withHeader("timestamp", "Odometer(meters)", "GPS coordinates", "Additional properties"));
            List csvRecords = csvParser.getRecords();


            new TripExtraction().extractTrip(csvRecords);

        } catch (MissingOptionException e) {

            new HelpFormatter().printHelp("Client", options);
            System.exit(1);

        } catch (ParseException e) {

            System.out.println("Failed parse the command.");
            System.exit(-1);

        } catch (IOException io) {

            System.out.println("Failed to load '*.csv'.");
            System.exit(-1);

        } catch (IllegalArgumentException ia) {
            System.out.println(ia.getMessage());
            System.exit(-1);
        }
    }
}
