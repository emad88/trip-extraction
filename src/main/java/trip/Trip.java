package trip;

import java.time.ZonedDateTime;

public class Trip {

    private ZonedDateTime startTime;
    private ZonedDateTime endTime;
    private Long distance = 0L;

    ZonedDateTime getStartTime() {
        return startTime;
    }

    void setStartTime(ZonedDateTime startTime) {
        this.startTime = startTime;
    }

    ZonedDateTime getEndTime() {
        return endTime;
    }

    void setEndTime(ZonedDateTime endTime) {
        this.endTime = endTime;
    }

    Long getDistance() {
        return distance;
    }

    void setDistance(Long distance) {
        this.distance = distance;
    }

    @Override
    public String toString() {
        return "Trip{" +
                "startTime=" + startTime +
                ", endTime=" + endTime +
                ", distance=" + distance +
                '}';
    }
}
