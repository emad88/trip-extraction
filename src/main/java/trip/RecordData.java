package trip;


import java.time.ZonedDateTime;

public class RecordData {

    private ZonedDateTime timestamp;
    private long Odometer;
    private GPSCoordinates gpsCoordinates;
    private OriginalOdometer additionalProperties;


    public ZonedDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(ZonedDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public long getOdometer() {
        return Odometer;
    }

    public void setOdometer(long odometer) {
        Odometer = odometer;
    }

    public GPSCoordinates getGpsCoordinates() {
        return gpsCoordinates;
    }

    public void setGpsCoordinates(GPSCoordinates gpsCoordinates) {
        this.gpsCoordinates = gpsCoordinates;
    }

    public OriginalOdometer getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(OriginalOdometer additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public String toString() {
        return "RecordData{" +
                "timestamp=" + timestamp +
                ", Odometer=" + Odometer +
                ", gpsCoordinates=" + gpsCoordinates +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}
