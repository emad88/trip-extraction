package trip;

public class OriginalOdometer{

    private long origOdo;

    public OriginalOdometer(long origOdo) {
        this.origOdo = origOdo;
    }

    public long getOrigOdo() {
        return origOdo;
    }


    @Override
    public String toString() {
        return "OriginalOdometer{" +
                "origOdo=" + origOdo +
                '}';
    }
}