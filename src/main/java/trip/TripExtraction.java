package trip;

import csv.CSVRecordParser;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class TripExtraction {

    private final static Logger LOGGER = LoggerFactory.getLogger(TripExtraction.class);

    public void extractTrip(List<CSVRecord> csvRecords) {
        List<RecordData> recordData = new CSVRecordParser().processCSVData(csvRecords);
        List<Trip> trips = processRecordData(recordData);
        printResult(trips);
    }

    // TODO written based on the pseudo code, has to refactor to make it more readable
    private List<Trip> processRecordData(List<RecordData> recordData) {
        RecordData previousRecord = null;
        List<Trip> trips = new ArrayList<>();
        Trip currentTrip = new Trip();
        Long distanceDifference;

        for (RecordData record : recordData) {
            if (previousRecord == null) {
                previousRecord = record;
                currentTrip.setStartTime(record.getTimestamp());
                continue;
            }

            distanceDifference = record.getOdometer() - previousRecord.getOdometer();
            if (TripUtil.isTimeDifferenceLessThanThreeMinutes(record.getTimestamp(), previousRecord.getTimestamp())) {
                if (distanceDifference > 0) {
                    currentTrip.setDistance(currentTrip.getDistance() + distanceDifference);
                }
            } else {
                if (currentTrip.getDistance() > 0) {
                    currentTrip.setEndTime(record.getTimestamp());
                    trips.add(currentTrip);
                    currentTrip = new Trip();
                    currentTrip.setStartTime(record.getTimestamp());
                }
            }
            previousRecord = record;
        }

        return trips;
    }

    private List<Trip> processRecordDataCorrect(List<RecordData> recordData) {
        //TODO
        return null;
    }

    private void printResult(List<Trip> trips) {
        trips.forEach(System.out::println);
        long sum = trips.stream()
                .mapToLong(Trip::getDistance)
                .sum();
        System.out.println("Sum before = "+ sum);
    }
}
