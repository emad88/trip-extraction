

# How to run the program

To run the program either run it with the provided jar file in the jar folder with this command:

```
java -jar trip-extraction.jar -f path-to-csv 
```

or run it inside the IDE using configuration, `program arguments`
``` 
-f path-to-csv
```

